Music recognition and streaming service

SETUP
*****
This setup assumes you have Python 2.7.* installed on an Ubuntu box.

1. Install virtualenvwrapper and create Moogle's virtual environment:

    sudo apt-get install virtualenvwrapper
    
    mkvirtualenv moogle
    

2. Install system dependencies:
    
    sudo apt-get install ffmpeg libfreetype6 libpng12-0 libpng12-dev libfreetype6-dev mysql-server libportaudio0 libportaudio2 libportaudiocpp0 portaudio19-dev virtualenvwrapper build-essential python-dev libblas-dev libatlas-base-dev liblapack3 liblapack-dev gfortran libagg-dev libmysqlclient-dev
    
    
3. Install project requirements in requirements.txt:

    pip install -r requirements.txt
    
4. Install matplotlib from source:
    
    cd /tmp/
    
    wget https://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.4.0/matplotlib-1.4.0.tar.gz
    
    tar -xzvf matplotlib-1.4.0.tar.gz
    
    cd matplotlib-1.4.0
    
    python setup.py install
    
    
5. Login to mysql and create main database and 'moogle' user for application.
    
    mysql -u root -p
    
    
    CREATE DATABASE IF NOT EXIST dejavu;
    
    
    CREATE USER 'moogle'@'localhost' IDENTIFIED BY 'mooglepassword';
    
    
    GRANT ALL PRIVILEGES ON dejavu . * TO 'moogle'@'localhost';
    
    
    FLUSH PRIVILEGES;    
    

6. Create database for app:


    cd <project directory>
    
    
    python manage.py syncdb
    
    
    
7. Install Apache2 and libapache2-mod-xsendfile.
    
    
    sudo apt-get install apache2 libapache2-mod-wsgi libapache2-mod-xsendfile



    

8. Run the development server and access the app on http://127.0.0.1:8000

    python manage.py runserver
    

9. To simulate the mobile app making a recognition request to the API, you can use curl. For example:


curl -F "/path/to/the/audio/file/recorded/" http://127.0.0.1:8000/api/tag/
