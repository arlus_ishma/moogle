from django.shortcuts import redirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib import messages

from django.conf import settings
from django.utils.encoding import smart_str
from django.http import HttpResponse

from .models import VerificationApplication, AudioFile, Purchases
from .forms import AudioFileForm
from .sampler import fingerprint_uploaded_file
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt


import os
import uuid
import warnings
import threading

warnings.filterwarnings("ignore")


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


def home(request):
    try:
        id = request.session['song_id']
        del request.session['song_id']
        return HttpResponseRedirect(reverse('download', args=[id]))
    except KeyError:
        pass
    tracks = AudioFile.objects.all()
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))


class VerificationApplicationCreate(LoginRequiredMixin, CreateView):
    model = VerificationApplication
    fields = ['firstname', 'lastname', 'email', 'identification', 'phone_number', 'residence', 'city',
              'agree_terms']

    def dispatch(self, *args, **kwargs):
        return super(VerificationApplicationCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(VerificationApplicationCreate, self).form_valid(form)


class VerificationApplicationDetails(LoginRequiredMixin, DetailView):
    model = VerificationApplication


class VerificationApplicationUpdate(LoginRequiredMixin, UpdateView):
    model = VerificationApplication
    fields = ['firstname', 'lastname', 'email', 'identification', 'phone_number', 'residence', 'city',
              'agree_terms']
    template_name_suffix = '_update_form'


class VerificationApplicationDelete(LoginRequiredMixin, DeleteView):
    model = VerificationApplication
    success_url = reverse_lazy('dashboard')


@login_required
def dashboard(request):
    form = AudioFileForm()
    my_tracks = AudioFile.objects.filter(user=request.user.id)
    total = 0
    earnings = Purchases.objects.filter(owner=request.user.id).values('price')
    for i in earnings:
        total += int(i["price"])
    try:
        obj = VerificationApplication.objects.get(user=request.user.id)
        is_verified = obj.verified
        applied = False if VerificationApplication.objects.filter(user=request.user).count() == 0 else True
        ver_id = obj.id
    except Exception:
        applied = False
        is_verified = False
    if applied and is_verified:
        # Add audio
        if request.method == 'POST':
            form = AudioFileForm(request.POST, request.FILES)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.user = User.objects.get(username=request.user)
                obj.save()
                fingerprint_uploaded_file(settings.UPLOAD_DIR)
                messages.add_message(request, messages.INFO, 'File uploaded successfully')
                return HttpResponseRedirect('/dashboard/')

                # To retain frontend widget, if form.is_valid() == False

        audio_form = form

    return render_to_response('dashboard.html', locals(), context_instance=RequestContext(request))


def logout(request):
    auth_logout(request)
    return redirect('/')


@csrf_exempt
def download_song(request, song_id):
    song = AudioFile.objects.get(id=song_id)
    purchase = Purchases(owner=song.user, song_name=song.name, artistes=song.artistes, price=song.price, song_id=song_id)
    purchase.save()
    path = AudioFile.objects.get(id=song_id).audio_file.url
    name = AudioFile.objects.get(id=song_id).name
    fsock = open(path, 'r')
    response = HttpResponse(fsock, mimetype='audio.mpeg')
    response['X-Sendfile'] = smart_str(path)
    response['Content-Type'] = "audio/mpeg"
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(name)
    response['Accept-Ranges'] = 'bytes'
    response['Content-Length'] = os.stat(path).st_size
    return response


@csrf_exempt
def buy_song(request, song_id):
    song = AudioFile.objects.get(id=song_id)
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "amount": song.price,
        "item_name": song.name + " - " + song.artistes,
        "invoice": str(uuid.uuid4().fields[-1])[:5],
        "notify_url": "http://178.62.209.46:8000/notify/",
        "return_url": "http://178.62.209.46:8000/download/" + song_id + "/",
        "cancel_return": "http://178.62.209.46:8000/cancel/",

    }
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render_to_response('payment-gateway.html', locals(), context_instance=RequestContext(request))


@csrf_exempt
def return_page(request, song_id):
    request.session['song_id'] = song_id
    return HttpResponseRedirect(reverse('home'))
