from django.contrib import admin
from .models import VerificationApplication, AudioFile, Tag, Purchases, Allocations


class VerificationApplicationAdmin(admin.ModelAdmin):
    pass


class AudioFileAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'genre')


class PurchasesAdmin(admin.ModelAdmin):
    list_display = ('owner', 'song_name', 'artistes', 'price')


admin.site.register(Tag)
admin.site.register(Purchases, PurchasesAdmin)
admin.site.register(Allocations)
admin.site.register(AudioFile, AudioFileAdmin)
admin.site.register(VerificationApplication, VerificationApplicationAdmin)