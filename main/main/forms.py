from django.forms import ModelForm
from .models import AudioFile, Tag
from django import forms


class AudioFileForm(ModelForm):
    audio_file = forms.FileField()

    class Meta:
        model = AudioFile
        fields = ['name', 'audio_file', 'genre', 'price', 'artistes', 'record_label']
        exclude = ('user', 'uploaded_on')


class PaymentForm(forms.Form):
    amount = forms.CharField(max_length=10)
    description = forms.CharField(max_length=100)
    reference = forms.CharField(max_length=10)
    email = forms.EmailField()

