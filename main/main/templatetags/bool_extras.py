__author__ = 'blaqx'

from django import template

register = template.Library()

@register.filter
def is_false(arg):
    return arg is False

@register.filter
def is_true(arg):
    return arg is True

