from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.defaults import page_not_found

from .views import VerificationApplicationCreate, VerificationApplicationUpdate, VerificationApplicationDelete, \
    VerificationApplicationDetails

admin.autodiscover()

urlpatterns = patterns('',
                       url('', include('social.apps.django_app.urls', namespace='social')),
                       url('', include('api.urls')),
                       url(r'^payments/', include('django_pesapal.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'main.views.home', name='home'),
                       url(r'^notify/', include('paypal.standard.ipn.urls')),
                       url(r'^dashboard/$', 'main.views.dashboard', name='dashboard'),
                       url(r'^submit-verification/$', 'main.views.dashboard', name='verify'),
                       url(r'^verification/apply/$', VerificationApplicationCreate.as_view(),
                           name='verification-apply'),
                       url(r'^verification/(?P<pk>\d+)/details/$', VerificationApplicationDetails.as_view(),
                           name='verification-details'),
                       url(r'^verification/(?P<pk>\d+)/update/$', VerificationApplicationUpdate.as_view(),
                           name='verification-update'),
                       url(r'verification/(?P<pk>\d+)/delete/$', VerificationApplicationDelete.as_view(),
                           name='verification-delete'),
                       url(r'^logout/$', 'main.views.logout', name='logout'),
                       url(r'^download/(?P<song_id>\d+)/$', 'main.views.download_song', name='download'),
                       url(r'^purchase-track/(?P<song_id>\d+)/$', 'main.views.buy_song', name='buy'),
                       #url(r'^notify', include('paypal.standard.ipn.urls')),
                       url(r'^(?P<song_id>\d+)/return', 'main.views.return_page', name="return-page"),
                       url(r'^uploads/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT})
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                                                                             document_root=settings.MEDIA_ROOT)
