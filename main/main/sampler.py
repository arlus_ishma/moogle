from dejavu import Dejavu


def fingerprint_uploaded_file(track_dir):
    config = {
        "database": {
        "host": "127.0.0.1",
        "user": "moogle",
        "passwd": "mooglepassword",
        "db": "dejavu",
        }
    }
    djv = Dejavu(config)
    djv.fingerprint_directory(track_dir, [".mp3"], 2)

