from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models.signals import post_save
import os
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import payment_was_successful


def notify(sender, **kwargs):
    import sys
    print "*************************"
    sys.stderr.write("***************************")
    pass

payment_was_successful.connect(notify)

class VerificationApplication(models.Model):
    user = models.ForeignKey(User, unique=True)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    email = models.EmailField()
    identification = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=20)
    residence = models.TextField()
    city = models.CharField(max_length=20)
    agree_terms = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    applied_on = models.DateTimeField(auto_now_add=True)
    verified_on = models.DateTimeField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('verification-details', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['applied_on']

    def __unicode__(self):
        return self.user.username

GENRES = (
    ('hihop', 'hihop'),
    ('rnb', 'rnb'),
    ('pop', 'pop'),
    ('rock', 'rock'),
    ('afrohouse', 'afrohouse'),
    ('gospel', 'gospel'),
    ('vernacular', 'vernacular'),
    ('bongo', 'bongo'),
    ('rhumba', 'rhumba'),
)


class AudioFile(models.Model):
    user = models.ForeignKey(User, related_name='audio_owner')
    audio_file = models.FileField(upload_to='tracks/', max_length=250)
    name = models.CharField(max_length=250)
    artistes = models.CharField(max_length=250)
    record_label = models.CharField(max_length=250)
    genre = models.CharField(max_length=10, choices=GENRES)
    price = models.CharField(max_length=5)
    uploaded_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    track = models.FileField(upload_to='tags/', max_length=250)

    def __unicode__(self):
        return self.track.url


class Purchases(models.Model):
    owner = models.ForeignKey(User)
    song_name = models.CharField(max_length=250)
    artistes = models.CharField(max_length=250)
    price = models.CharField(max_length=5)
    song_id = models.IntegerField()

    def __unicode__(self):
        return self.song_name



class Allocations(models.Model):
    moogle = models.IntegerField(default=0)
    owners = models.IntegerField(default=0)


    def __unicode__(self):
        return ("Moogle: %s - Artistes %s") %(self.moogle, self.owners)

def add_allocations(sender, **kwargs):
    if kwargs['created']:
        if issubclass(sender, Purchases):
            price = kwargs['instance'].price
            moogle = 0.15 * int(price)
            owners = 0.85 * int(price)
            try:
                n = Allocations.objects.get(id=1)
                n.moogle += moogle
                n.owners += owners
                n.save()
            except Exception:
                n = Allocations(moogle=moogle, owners=owners)
                n.save()

post_save.connect(add_allocations, dispatch_uid="add_allocation")
