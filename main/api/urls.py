from django.conf.urls import *
from .api import TagResource

tag_resource = TagResource()

urlpatterns = patterns('',
                       (r'^api/', include(tag_resource.urls)),

)
