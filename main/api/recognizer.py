from dejavu import Dejavu
from dejavu.recognize import FileRecognizer


def recognize(tag_path):
    config = {
        "database": {
        "host": "127.0.0.1",
        "user": "moogle",
        "passwd": "mooglepassword",
        "db": "dejavu",
        }
    }
    djv = Dejavu(config)
    song = djv.recognize(FileRecognizer, tag_path)
    return song