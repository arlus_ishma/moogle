import warnings

from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from tastypie import fields

from main.models import Tag
from .recognizer import recognize


warnings.filterwarnings("ignore")


class MultipartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')

        if format == 'application/x-www-form-urlencoded':
            return request.POST

        if format.startswith('multipart'):
            data = request.POST.copy()
            data.update(request.FILES)

            return data

        return super(MultipartResource, self).deserialize(request, data, format)


class TagResource(MultipartResource, ModelResource):
    track = fields.FileField(attribute="track", null=True, blank=True)
    class Meta:
        queryset = Tag.objects.all()
        resource_name = 'tag'
        authorization = Authorization()
        object_class = Tag
        always_return_data = True

    def dehydrate(self, bundle):
        bundle.obj.save()
        import sys
        sys.stderr.write(str(bundle))
        result = recognize(bundle.obj.track)
        bundle.data['tag'] = result
        return bundle

    #def obj_create(self, bundle, **kwargs):
    #    track = bundle.data['track']
    #    bundle.obj = Tag(track=track)
    #    bundle.obj.save()
    #    result = recognize(bundle.track)
    #    bundle.data['tag'] = result
    #    return bundle

    #def hydrate(self, bundle):
    #    import sys
    #    sys.stderr.write(str(bundle.data))
    #    bundle.obj.track = bundle.data['track']
    #    bundle.obj.save()
    #    result = recognize(bundle.obj.track)
    #    bundle.data['tag'] = result
    #    return bundle

    #def obj_create(self, bundle, **kwargs):
    #    result = recognize(bundle.obj.track)
    #    bundle.data['tag'] = result
    #    return super(TagResource, self).obj_create(bundle, **kwargs)

    #def hydrate(self, bundle):
    #    fil = bundle.obj.track
    #    import sys
    #    sys.stderr.write(str(fil))
    #    result = recognize(fil)
    #    bundle.data['tag'] = result
    #    return bundle
