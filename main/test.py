from dejavu import Dejavu
from dejavu.recognize import MicrophoneRecognizer
import warnings
import json

warnings.filterwarnings("ignore")

config = {"database": {"host": "127.0.0.1", "user": "moogle", "passwd": "mooglepassword", "db": "dejavu"}}
djv = Dejavu(config)
song = djv.recognize(MicrophoneRecognizer, seconds=10)
print song
